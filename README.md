# Sample test cases

This project provides sample [ManTester](https://mantester.com) test cases.

You can use it to load the test cases into a test run.
