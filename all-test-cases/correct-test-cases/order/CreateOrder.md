## `OR-100` Create order

Orderer creates new order.

!Core @Orders

###

- Log in as `orderer@testers.com`.
- Navigate to Create order page.
- Enter Customer: Some customer.
- Enter Delivery date: 1 week in the future.
- Create the order.
- Verify new order created.
- Verify order data match all the values entered.
