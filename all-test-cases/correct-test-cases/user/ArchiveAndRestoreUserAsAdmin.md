## `US-201` Archive and restore user as Admin

Admin archives and restores user.

!Enabling @Users

###

- Log in as Admin.
- Navigate to Users page.
- Find user with Email `user@testers.com`.
- Archive the user.
- Verify user Status is archived.
- Restore the user.
- Verify user Status is active.
