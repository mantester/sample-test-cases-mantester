## `US-200` Edit user as Admin

Admin edits user.

!Enabling @Users

###

- Log in as Admin.
- Navigate to Users page.
- Find user with Email `user@testers.com`.
- Enter new email | Email
- Enter new full name | Full name
- Save user data.
- Verify user data match the all values entered.
