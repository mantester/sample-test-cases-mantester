## `US-100` Create user as Admin

!Core @Users

###

- Log in as Admin.
- Navigate to Create user page.
- Enter Email: `new@testers.com`.
- Enter Password: `password`.
- Enter Full name: New User.
- Create the user.
- Verify the newly created user is displayed.
- Verify user data match all the values entered.
- Verify user Status is active.
- Log in as the newly created user.
