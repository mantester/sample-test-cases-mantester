## `US-001` Login as Admin

Successfully log in as Admin user.

!Smoke

@Users @Login

### Steps

- Navigate to Login page.
- Enter Email: `admin@testers.com`.
- Enter Password: `password`.
- Log in.
- Verify Home page displayed.
- Verify current user is Test Admin.
