## Test 1 without code

!Core @Users

###

- Log in as Admin.
- Navigate to Create user page.
- Enter data.
- Create the user.


## Test 2 without code

!Core @Users

###

- Log in as Root.
- Navigate to Create user page.
- Enter data.
- Create the user.


## `US-001` Test without category

@Users

###

- Navigate to Create user page.
- Enter data.
- Create the user.


## `US-001` Test with duplicate code

!Smoke

@Users @Login

### Steps

- Navigate to Login page.
- Log in.


## `US-300` Test with triplicate code

!Smoke  @Users

### Steps

- Navigate to Login page.


## `US-300` Test with triplicate code

!Smoke  @Users

### Steps

- Navigate to Login page.


## `US-300` Test with triplicate code

!Smoke  @Users

### Steps

- Navigate to Login page.


## `US-201` Test with no groups

!Smoke

###

- Navigate to Login page.
- Enter data.
- Log in.


## Empty test


## `OR-100` Valid test

Orderer creates new order.

!Core @Orders

###

- Log in as `orderer@testers.com`.
- Navigate to Create order page.
- Create the order.


## Test with duplicate implicit code

!Core

###

- Log in as `orderer@testers.com`.
- Navigate to Create order page.


## Test with duplicate implicit code

!Core

###

- Log in as `orderer@testers.com`.
